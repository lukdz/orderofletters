#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

void swap(char *text){
    if( strlen(text)>3 && text[0]!='/'){
        int rand_a, rand_b;
        char temp[100][3];
        int letter_count=0;
        for(int i=0; text[i]!='\0'; letter_count++){
            if( ((text[i]=='c' || text[i]=='r' || text[i]=='s') && text[i+1]=='z')      //cz,rz,sz
                || ((text[i]=='C' || text[i]=='R' || text[i]=='S') && text[i+1]=='z')   //Cz,Rz,Sz
                || (text[i]=='c' && text[i+1]=='h')     //ch)
                || (text[i]=='C' && text[i+1]=='h')     //Ch)
                || text[i]<=0 ){    //ą, ć, ę, ł, ń, ó, ś, ż, ź
                    snprintf(temp[letter_count], 3, "%s", &text[i]);
                    i+=2;
            }
            else{
                snprintf(temp[letter_count], 2, "%1s", &text[i]);
                i+=1;
            }
        }
        for(int i=0; i<strlen(text); i++){
            rand_a = rand() % ( letter_count-2 ) + 1;
            rand_b = rand() % ( letter_count-2 ) + 1;
            sprintf(temp[99], "%s", temp[rand_a]);
            sprintf(temp[rand_a], "%s", temp[rand_b]);
            sprintf(temp[rand_b], "%s", temp[99]);
        }
        int poz=0;
        for(int i=0; i<letter_count; i++){
            poz += sprintf(&text[poz], "%s", temp[i]);
        }
    }
}

void mprintf(char *type, char *text){
    if(text[0]=='/'){
        printf(type, &text[1]);
    }
    else
    {
        printf(type, text);
    }
}

int main (int argc, char *argv[])
{
    if (argc != 3)
    {
        printf ("Podaj nazwę pliku źródłowego, oraz tryb wyświetlania tekstu:\n");
        printf ("1 - oryginał\n");
        printf ("2 - zmieniona kolejnośc liter\n");
        printf ("3 - oba\n");
        printf ("Wyrazy, w których kolejnośc liter ma pozostać niezmieniona poprzedź tym /znakiem np. \"/password\".\n");
        return 0;
    }

    FILE *fp;
    fp=fopen(argv[1], "r");
    char text[100];
    int mode;
    sscanf(argv[2], "%d", &mode);
    if(mode==1 || mode==3)
    {
        printf ("\nOryginał\n");
        while(fscanf(fp, "%99s", text) != EOF)
        {
            mprintf("%s ", text);
        }
    }

    if(mode==2 || mode==3){
        printf ("\n\nZmieniona kolejnośc liter\n");
        rewind(fp);
        time_t t;
        srand((unsigned) time(&t));
        while(fscanf(fp, "%99s", text) != EOF)
        {
            swap(text);
            mprintf("%s ", text);
        }
    }

    printf ("\n");
    return 0;
}
