all: main char_to_int

main: main.c 
	gcc -o main main.c -I.

char_to_int: char_to_int.c 
	gcc -o char_to_int char_to_int.c -I.
